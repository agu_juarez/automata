package automata;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashSet;
import java.util.Set;

import utils.Pair;
import utils.Triple;

import static utils.WordUtils.*;
public class RegexParser {
	
	public  Set<Character> regexAlph = new HashSet<Character>();
	
	
	public RegexParser() {
		for(int i =97; i<=122;i++) {
			regexAlph.add((char)i);
		}
	}
	
	/* @param path Path to the input file containing the searching text.
	 * @return the number of first regex matching line. if no matching, it returns 0.
	 * @throws Exception Throws an exception if regex is not a well-formed 
	 * regular expression.
	 */
	public  int mygrep(String regex, String path) throws RegexException, Exception{

		int numberLine = 1;
		DFA dfa = fromRegexToDFA(regex);
		
		if (dfa != null) { //well-formed regex
			DFA gdfa = dfa.toGeneralDFA(regexAlph);
			
			File archivo = new File(path);
			FileReader fr = new FileReader(archivo);
			BufferedReader br = new BufferedReader(fr);
			
			String line;
			while ((line = br.readLine()) != null) {
				if (gdfa.accepts(line))
					return numberLine;
				numberLine++;
			}
			
		}else 
			throw new RegexException("bad-formed regular expression");
	
		return 0;	
	 }
	
	 /* E->T E'
	  * E'-> '|' T E' |lambda
	  * T->F T'
	  * T'->. F T' |lambda
	  * F ->P H
	  * H-> * | lambda
	  * P-> (E) | L
	  * L -> a | b | c |...|z
	  */
	
     public DFA fromRegexToDFA(String w) {
		String word =prepareWord(w);
		Pair r = e(word); //llamada al parser descendente recursivo
		boolean b = r!=null && (token(r.getCurrentWord())=='#');
		if (b) {
			return r.getAf();
		}
		return null;
		
	}
    
    // E->T E'
    private Pair e(String word) {
     	if (regexAlph.contains(token(word)) || token(word) == '(') {
     		Pair pairT = t(word);
     		if (pairT == null) return null;
     		
     		Pair pairEp = ep(pairT.getCurrentWord());
     		if (pairEp == null) return null;
     		
     		if (pairEp.getAf() == null)
     			return (new Pair(pairT.getAf(), pairEp.getCurrentWord()));
     		
     		return (new Pair(pairT.getAf().concat(pairEp.getAf()) , pairEp.getCurrentWord()));
     	}
     	
     	return null;
    }
    
    // P-> (E) | L
    private Pair p(String word) {
    	if (token(word) == '(') {
    		Pair pairE = e(moveForward(word));
    		if (pairE == null) return null;
    		
    		if (token(pairE.getCurrentWord()) == ')')
    			return (new Pair(pairE.getAf() ,pairE.getCurrentWord()));
    		
    		return null;
    	}
    	
    	if (regexAlph.contains(token(word))) {
    		Pair pairL = l(word);
    		if (pairL == null) return null;
    		
    		return (new Pair(pairL.getAf(), pairL.getCurrentWord()));
    	}
    	
    	return null;
    }
    
    // E'-> '|' T E' |lambda
    private Pair ep(String word) {
    	if (token(word) == '|') {
    		Pair pairT = t(moveForward(word));
    		if (pairT == null) return null;
    		
    		Pair pairEp = ep(pairT.getCurrentWord());
    		if (pairEp == null) return null;
    		
    		if (pairEp.getAf() == null)
    			return (new Pair(pairT.getAf(), pairEp.getCurrentWord()));
    		
    		return (new Pair(pairT.getAf().union(pairEp.getAf()), pairEp.getCurrentWord()));
    	}
    	
    	if (token(word) == ')' || token(word) == '#')
    		return (new Pair(null, word));
    	
    	return null;
    }
    
    // T->F T'
    private Pair t(String word) {
    	if (regexAlph.contains(token(word)) || token(word) == '(') {
    		Pair pairF = f(word);
    		if (pairF == null) return null;
    		
    		Pair pairTp = tp(pairF.getCurrentWord());
    		if (pairTp == null) return null;
    		
    		if (pairTp.getAf() == null)
    			return (new Pair(pairF.getAf(), pairTp.getCurrentWord()));
    		
    		return (new Pair(pairF.getAf().concat(pairTp.getAf()), pairTp.getCurrentWord()));
    	}
    	
    	return null;
    }
    
    // F ->P H
    private Pair f(String word) {
    	if (regexAlph.contains(token(word)) || token(word) == '(') {
    		Pair pairP = p(word);
    		if (pairP == null) return null;
    		
    		Pair pairH = h(pairP.getCurrentWord());
    		if (pairH == null) return null;
    		
    		if (pairH.getAf() == null)
    			return (new Pair(pairP.getAf(), pairH.getCurrentWord()));
    		
    		return (new Pair(pairP.getAf().star(), pairH.getCurrentWord()));
    	}
   
    	return null;
    }
    
    // H-> * | lambda
    private Pair h(String word) {
    	if (token(word) == '*') {
    		State s = new State("stateClausure", true, true);
    		Set<State> states = new HashSet<State>();
    		Set<Character> alphabet = new HashSet<Character>();
    		Set<Triple<State, Character, State>> transitions = new HashSet<Triple<State, Character, State>>();
    		states.add(s);
    		DFA myDfa = new DFA(states, alphabet, transitions);
    		return (new Pair(myDfa, moveForward(word)));
    	}
    	if (token(word) == ')' || token(word) == '#' || token(word) == '.' || token(word) == '|')
    		return (new Pair(null, word));
    	
    	return null;
    }
    
   
    // T'->. F T' |lambda
    private Pair tp(String word) {
    	if (token(word) == '.') {
    		Pair pairF = f(moveForward(word));
    		if (pairF == null) return null;
    		
    		Pair pairTp = tp(pairF.getCurrentWord());
    		if (pairTp == null) return null;
    		
    		if (pairTp.getAf() == null)
    			return (new Pair(pairF.getAf(), pairTp.getCurrentWord()));
    		
    		return (new Pair(pairF.getAf().concat(pairTp.getAf()), pairTp.getCurrentWord()));
    	}
    	
    	if (token(word) == ')' || token(word) == '#' || token(word) == '|')
    		return (new Pair(null, word));
    	
    	
    	return null;
    }
     
    // L -> a | b | c |...|z
    private Pair l(String word) {
    	if (regexAlph.contains(token(word))) { 
    		return (new Pair(DFA.fromToken(token(word)), moveForward(word)));
    	}
    	 
    	return null;
    }
   
}
