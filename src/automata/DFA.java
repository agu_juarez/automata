package automata;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.Map.Entry;
import utils.Triple;

/* Implements a DFA (Deterministic Finite Automata).
*/
public class DFA extends FA {

	/*
	 * Construction
	 */

	// Constructor

	public DFA(Set<State> states, Set<Character> alphabet, Set<Triple<State, Character, State>> transitions)
			throws IllegalArgumentException {
		// TODO
		// Triple<State,Character,State> secondArray = transitions.toArray();
		if (states.size() == 0)
			throw new IllegalArgumentException("The state set can not be empty");
		
		this.states = states;
		this.alphabet = alphabet;
		HashMap<State, HashMap<Character, Set<State>>> transi = new HashMap<State, HashMap<Character, Set<State>>>();

		for (Triple<State, Character, State> t : transitions) {
			State first = t.first();
			Character second = t.second();
			State third = t.third();

			if (!transi.containsKey(first)) {
				Set<State> s = new HashSet<State>();
				HashMap<Character, Set<State>> m = new HashMap<Character, Set<State>>();
				s.add(third);
				m.put(second, s);
				transi.put(first, m);
			} else {
				if (transi.get(first).containsKey(second)) {
					transi.get(first).get(second).add(third);
				} else {
					Set<State> s = new HashSet<State>();
					s.add(third);
					transi.get(first).put(second, s);
				}
			}
		}
		
		this.delta = transi;
		if (!repOk())
			throw new IllegalArgumentException("The invariant is not fulfilled");
	}
	
	/**
	 * 
	 * @param Set<State> states
	 * @return boolean, if the set states contains at least one final state
	 */
	private boolean containsStateFinal(Set<State> states) {
		for (State s : states) {
			for (State stateFinal : finalStates())
				if (s.equals(stateFinal)) return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param Set<State> states
	 * @param Character c
	 * @return Set<State>, the set of states that i move from states to c
	 */
	private Set<State> movements(Set<State> states, Character c) {
		Set<State> statesMovements = new HashSet<State>();
		for (State s : states) {
			if (this.delta.get(s) != null && this.delta.get(s).get(c) != null)
				statesMovements.addAll(this.delta.get(s).get(c));
		}
		
		return statesMovements;
	}

	// Check if a Set <State> contains a State s
	private boolean checkStateRepeat(State s, Set<State> states) {
		for (State st : states) {
			if (s.getName().equals(st.getName()))
				return true;
		}
		return false;
	}

	// Convert the delta into a Set <Triple <State, Character, State >>
	private Set<Triple<State, Character, State>> convertDelta(HashMap<State, HashMap<Character, Set<State>>> del) {
		Set<Triple<State, Character, State>> transitions = new HashSet<Triple<State, Character, State>>();

		for (Entry<State, HashMap<Character, Set<State>>> entry : del.entrySet()) {
			for (Character c : entry.getValue().keySet()) {
				for (State s : entry.getValue().get(c)) {
					transitions.add(new Triple(entry.getKey(), c, s));
				}
			}
		}

		return transitions;

	}

	/*
	 * State querying
	 */

	@Override
	public String toDot() {
		// First part: Write the two initial lines of the .dot file and the
		// States initials
		String firstPart = "digraph {" + "\n" + "    inic[shape=point];" + "\n" + "    inic->"
				+ initialState().getName() + ";" + "\n";

		// Second part: Write all transitions
		String secondPart = "";
		for (Entry<State, HashMap<Character, Set<State>>> entry : delta.entrySet()) {
			for (Entry<Character, Set<State>> entryAux : entry.getValue().entrySet()) {
				for (State state : entryAux.getValue())
					secondPart = "    " + entry.getKey().getName() + "->" + state.getName() + " [label=\""
							+ entryAux.getKey().toString() + "\"];" + "\n" + secondPart;
			}
		}

		// Third part: Write all the final statements
		String thirdPart = "";
		Set<State> finalStates = finalStates();
		for (State s : finalStates)
			thirdPart = "    " + s.getName() + "[shape=doublecircle];" + "\n" + thirdPart;
		thirdPart = thirdPart + "}";

		return (firstPart + secondPart + thirdPart);
	}

	/*
	 * Automata methods
	 */

	@Override
	public boolean accepts(String string) {
		if (string.length() == 0) return false;

		if (!verifyString(string)) return false;

		int cont = 1;
		LinkedList<Set<State>> conjuntStates = new LinkedList<Set<State>>();
	
		Set<State> statesCurrent = new HashSet<State>();
		statesCurrent.add(initialState());	
		conjuntStates.add(movements(statesCurrent, string.charAt(0)));

		while (cont < string.length()) {
			Set<State> setCurrent = conjuntStates.get(0);
			Set<State> movementsCurrent = movements(setCurrent, string.charAt(cont));
			conjuntStates.remove(0);
			conjuntStates.add(movementsCurrent);
			cont++;
		}

		if (containsStateFinal(conjuntStates.get(0))) return true;

		return false;
	}

	/**
	 * Converts the automaton to a NFA.
	 * 
	 * @return NFA recognizing the same language.
	 */
	public NFA toNFA() {
		Set<Triple<State, Character, State>> transitions = new HashSet<Triple<State, Character, State>>();
		for (Entry<State, HashMap<Character, Set<State>>> entry : delta.entrySet()) {
			HashMap<Character, Set<State>> map = entry.getValue();
			for (Character c : map.keySet()) {
				for (State s : map.get(c)) {
					transitions.add(new Triple(entry.getKey(), c, s));
				}
			}
		}
		return new NFA(this.states, this.alphabet, transitions);
	}

	/**
	 * Converts the automaton to a NFALambda.
	 * 
	 * @return NFALambda recognizing the same language.
	 */
	public NFALambda toNFALambda() {
		Set<Triple<State, Character, State>> transitions = new HashSet<Triple<State, Character, State>>();
		for (Entry<State, HashMap<Character, Set<State>>> entry : delta.entrySet()) {
			HashMap<Character, Set<State>> map = entry.getValue();
			for (Character c : map.keySet()) {
				for (State s : map.get(c)) {
					transitions.add(new Triple(entry.getKey(), c, s));
				}
			}
		}
		return new NFALambda(this.states, this.alphabet, transitions);
	}

	/**
	 * Checks the automaton for language emptiness.
	 * 
	 * @returns True iff the automaton's language is empty.
	 */
	public boolean isEmpty() {
		Set<State> statesCurrent = new HashSet<State>();
		Set<State> statesAux = new HashSet<State>();

		statesCurrent.add(initialState());
		do {
			for (State s : statesCurrent) {
				if (delta.get(s) != null) {
					for (Character c : delta.get(s).keySet()) {
						statesAux.addAll(delta.get(s).get(c));
					}
				}

				if (containsStateFinal(statesAux)) return false;

				statesCurrent.removeAll(statesCurrent);
				statesCurrent.addAll(statesAux);
				statesAux.removeAll(statesAux);
			}
		} while (!statesCurrent.isEmpty());

		return true;
	}

	/**
	 * Checks the automaton for language infinity.
	 * 
	 * @returns True iff the automaton's language is finite.
	 */

	public boolean isFinite() {

		Set<State> statesCurrent = new HashSet<State>();
		Set<State> statesAux = new HashSet<State>();

		for (State st : states) {
			statesCurrent.add(st);
			do {
				for (State s : statesCurrent) {
					if (delta.get(s) != null) {
						for (Character c : delta.get(s).keySet()) {
							statesAux.addAll(delta.get(s).get(c));
						}
					}

					if (checkStateRepeat(st, statesAux))
						return false;
				}
				statesCurrent.removeAll(statesCurrent);
				statesCurrent.addAll(statesAux);
				statesAux.removeAll(statesAux);
			} while (!statesCurrent.isEmpty());
		}
		return true;
	}

	/**
	 * Returns a new automaton which recognizes the complementary language.
	 * 
	 * @returns a new DFA accepting the language's complement.
	 */
	public DFA complement() {

		for (State s : states) {
			if (!s.isFinal())
				s.setFinal(true);
			else
				s.setFinal(false);
		}

		Set<Triple<State, Character, State>> transitions = new HashSet<Triple<State, Character, State>>();

		for (Entry<State, HashMap<Character, Set<State>>> entry : delta.entrySet()) {
			HashMap<Character, Set<State>> map = entry.getValue();
			for (Character c : map.keySet()) {
				for (State s : map.get(c)) {
					transitions.add(new Triple(entry.getKey(), c, s));
				}
			}
		}

		return new DFA(states, alphabet, transitions);
	}

	/**
	 * Returns a new automaton which recognizes the kleene closure of language.
	 * 
	 * @returns a new DFA accepting labguajes's closure.
	 */
	public DFA star() {
		Set<State> stateInitial = new HashSet<State>();
		stateInitial.add(initialState());

		for (State state : finalStates()) {
			HashMap<Character, Set<State>> newDelta = new HashMap<Character, Set<State>>();
			newDelta.put(this.Lambda, stateInitial);
			this.delta.put(state, newDelta);
		}

		NFALambda myNfaLambda = new NFALambda(this.states, this.alphabet, convertDelta(this.delta));

		return myNfaLambda.toDFA();
	}

	/**
	 * Verify if there is a different initial state of the name parameter, if this
	 * happens to it, set it to false
	 * 
	 * @param states
	 * @param name
	 */
	private void setStateInitial(Set<State> states, String name) {
		for (State s : states)
			if (s.isInitial() && !(s.getName().equals(name)))
				s.setInitial(false);
	}

	/**
	 * Returns a new automaton which recognizes the union of both languages, the one
	 * accepted by 'this' and the one represented by 'other'.
	 * 
	 * @returns a new DFA accepting the union of both languages.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public DFA union(DFA other) {
		HashMap<State, HashMap<Character, Set<State>>> deltaUnion = new HashMap<State, HashMap<Character, Set<State>>>();
		Set<State> statesUnion = new HashSet<State>();
		Set<Character> alphabetUnion = new HashSet<Character>();
		Set<Triple<State, Character, State>> transitionsUnion = new HashSet<Triple<State, Character, State>>();

		// create new initial state of the union and we put it in the joint
		State initialState = new State("qi", true, false);
		statesUnion.add(initialState);

		// Union of the states of both automatas
		statesUnion.addAll(this.states);
		statesUnion.addAll(other.states);
		for (State fnlSt : this.finalStates())
			for (State fnlSt2 : other.finalStates()) 
				if (!fnlSt.equals(fnlSt2))
					for(State chgStUnion : statesUnion)
						if (chgStUnion.equals(fnlSt2))
							chgStUnion.setFinal(true);

		// Put the delta of the first automata in the deltaUnion
		deltaUnion.putAll(this.delta);
		
		// Add values ​​for new alphabet
		alphabetUnion.addAll(this.alphabet);
		alphabetUnion.addAll(other.alphabet);

		// Put the delta of the second automata in the deltaUnion
		for (Entry<State, HashMap<Character, Set<State>>> entry : other.delta.entrySet()) {
			if (deltaUnion.containsKey(entry.getKey())) {
				HashMap<Character, Set<State>> value = entry.getValue();
				for (Character c : value.keySet()) {
					if (deltaUnion.get(entry.getKey()).containsKey(c)) 
						deltaUnion.get(entry.getKey()).values().addAll(value.values());
					else
						deltaUnion.get(entry.getKey()).put(c, value.get(c));
				}
			}
		}

		// create a transition that goes from the new initial state by Lambda to the
		// initial states of the automatas
		HashMap<Character, Set<State>> newDelta = new HashMap<Character, Set<State>>();
		Set<State> st = new HashSet<State>();
		st.add(this.initialState());
		st.add(other.initialState());

		newDelta.put(this.Lambda, st);
		deltaUnion.put(initialState, newDelta);

		// get the initial states of both automatas
		setStateInitial(statesUnion, initialState.getName());

		// passed the deltaUnion toSet<Triple>
		transitionsUnion = convertDelta(deltaUnion);

		// As it is an NFALambda we pass it to DFA
		NFALambda myNfaLambda = new NFALambda(statesUnion, alphabetUnion, transitionsUnion);
		return myNfaLambda.toDFA();
	}
	
	/**
	 * Returns a new automaton which recognizes the char a
	 * @returns a new DFA accepting the char a.
	 */	

	public static DFA fromToken(char a) {
		Set<State> states = new HashSet<State>();
		Set<Character> alphabet = new HashSet<Character>();
		Set<Triple<State, Character, State>> transitions = new HashSet<Triple<State, Character, State>>();
		
		State stateInitial = new State("q0", true, false);
		State stateFinal = new State("q1", false, true);
		
		states.add(stateInitial);
		states.add(stateFinal);
		
		alphabet.add(a);
		
		transitions.add(new Triple<State, Character, State>(stateInitial, a, stateFinal));
		
		return (new DFA(states, alphabet, transitions));
	}
	
	/**
	 * Returns a new automaton which recognizes the concatenation of both
	 * languages, the one accepted by 'this' and the one represented
	 * by 'other'. 
	 * 
	 * @returns a new DFA accepting the concatenation of both languages.
	 */	
	public DFA concat(DFA other) {
		assert repOk();
		assert other.repOk();
		HashMap<State, HashMap<Character, Set<State>>> deltaConcat = new HashMap<State, HashMap<Character, Set<State>>>();
		Set<State> concatState = new HashSet<State>();
		Set<Character> alphabetConcat = new HashSet<Character>();
		Set<Triple<State, Character, State>> transitionsConcat = new HashSet<Triple<State, Character, State>>();

		// create new initial state of the concat and we put it in the joint
		State initermediateState = new State("qi", false, false);
		concatState.add(initermediateState);

		// Put all states from us automatas on a new set of states
		concatState.addAll(this.states);
		concatState.addAll(other.states);
		
		// Put the delta on deltaConcat
		deltaConcat.putAll(this.delta);
		
		// Put the delta of the second automata in the deltaConcat
		for (Entry<State, HashMap<Character, Set<State>>> entry : other.delta.entrySet()) {
			if (deltaConcat.containsKey(entry.getKey())) {
				HashMap<Character, Set<State>> value = entry.getValue();
				for (Character c : value.keySet()) {
					if (!deltaConcat.get(entry.getKey()).containsKey(c))
						deltaConcat.get(entry.getKey()).put(c, value.get(c));
				}
			}
		}
		
		// Add values ​​for new alphabet
		alphabetConcat.addAll(this.alphabet);
		alphabetConcat.addAll(other.alphabet);

		// create lambda transition that goes from this final states to qi state,
		// and a lambda transition that go from qi to other initial  state
		HashMap<Character, Set<State>> newDelta = new HashMap<Character, Set<State>>();
		Set<State> st = new HashSet<State>();
		if(concatState.contains(other.initialState())) {
			State otherInitialState = new State(other.initialState().getName(),false,other.initialState().isFinal());
			st.add(otherInitialState);
		}else {
			st.add(other.initialState());
		}

		newDelta.put(Lambda, st);
		deltaConcat.put(initermediateState, newDelta);
		
		HashMap<Character, Set<State>> newSecondDelta = new HashMap<Character, Set<State>>();
		Set<State> secondst = new HashSet<State>();
		secondst.add(initermediateState);
		for (State fnlSt : this.finalStates()) {
			deltaConcat.get(fnlSt).put(Lambda, secondst);
		}
		
		//Change States to Final or Initial when this need change
		for (State fnlSt : this.finalStates())
			for (State fnlSt2 : other.finalStates()) 
				if (!fnlSt.equals(fnlSt2))
					for(State chgStConcat : concatState) {
						if (chgStConcat.equals(fnlSt2))
							chgStConcat.setFinal(true);
						if (chgStConcat.equals(fnlSt))
							chgStConcat.setFinal(false);
						if ((!chgStConcat.equals(this.initialState())) && chgStConcat.equals(other.initialState())) 
							chgStConcat.setInitial(false);
					}
		
		// get the initial states of both automatas
		setStateInitial(concatState, this.initialState().getName());

		// passed the deltaConcat toSet<Triple>
		transitionsConcat = convertDelta(deltaConcat);

		// As it is an NFALambda we pass it to DFA
		NFALambda myNfaLambda = new NFALambda(concatState, alphabetConcat, transitionsConcat);

		return myNfaLambda.toDFA();
	}
	
	
	/**
	 * Returns a new automaton which recognizes the following language:
	 * L(sigma*) . L(this) . L(sigma*)  where * is the start operation 
	 * and . concat operation
	 * @returns a new DFA accepting the described language.
	 */	

	public DFA toGeneralDFA(Set<Character> sigma) {
    	LinkedList<Character> listCharacter = new LinkedList<Character>();
    	
    	for (Character c : sigma) 
    		listCharacter.add(c);
    	
    	DFA afdSigma = fromToken(listCharacter.removeFirst());
    	
    	while (listCharacter.size() != 0) {
    		afdSigma = afdSigma.union(fromToken(listCharacter.removeFirst()));
    	}
    	
    	DFA dfaSigmaClausure = afdSigma.star();
    	
    	DFA dfaFinal = dfaSigmaClausure.concat(this);
    	
    	return (dfaFinal.concat(dfaSigmaClausure));
    }

	@Override
	public boolean repOk() {
		// TODO: Check that the alphabet does not contains lambda.
		// TODO: Check that one and just one state is marked to be a initial state.
		// TODO: Check that all transitions are correct. All states and characters
		// should be part of the automaton set of states and alphabet.
		// TODO: Check that the transition relation is deterministic.State[] auxArray =
		// (State[]) states.toArray();
		int cont = 0;
		for (State s : states) {
			if (s.isInitial())
				cont++;
		}
		if (cont == 0 || cont > 1)
			return false;

		// Verify that the transitions are valid (States and valid character)
		for (Entry<State, HashMap<Character, Set<State>>> entry : delta.entrySet()) {
			if (!states.contains(entry.getKey()))
				return false;
			for (Character c : entry.getValue().keySet()) {
				if (!c.equals(this.Lambda)) {
					if (!alphabet.contains(c))
						return false;
					for (Set<State> s : entry.getValue().values()) {
						if (s.size() > 1)
							return false;
						for (State state : s)
							if (!states.contains(state))
								return false;
					}
				} else {
					return false;
				}
			}
		}

		return true;
	}

}
