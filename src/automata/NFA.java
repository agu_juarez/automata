package automata;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;

import utils.Triple;

public class NFA extends FA {

	/*
	 * Construction
	 */

	// Constructor
	public NFA(Set<State> states, Set<Character> alphabet, Set<Triple<State, Character, State>> transitions) throws IllegalArgumentException {
		if (states.size() == 0) throw new IllegalArgumentException("The state set can not be empty");

		this.states = states;
		this.alphabet = alphabet;
		HashMap<State, HashMap<Character, Set<State>>> transi = new HashMap<State, HashMap<Character, Set<State>>>();

		for (Triple<State, Character, State> t : transitions) {
			State first = t.first();
			Character second = t.second();
			State third = t.third();

			if (!transi.containsKey(first)) {
				Set<State> s = new HashSet<State>();
				HashMap<Character, Set<State>> m = new HashMap<Character, Set<State>>();
				s.add(third);
				m.put(second, s);
				transi.put(first, m);
			} else {
				if (transi.get(first).containsKey(second)) {
					transi.get(first).get(second).add(third);
				} else {
					Set<State> s = new HashSet<State>();
					s.add(third);
					transi.get(first).put(second, s);
				}
			}
		}
	
		this.delta = transi;

		if (!repOk()) {
			throw new IllegalArgumentException("The invariant is not fulfilled");
		}
	}
	
	/**
	 * 
	 * @param Set<State> states
	 * @return String, belonging to the name of the new state
	 */
	private String generateName(Set<State> states) {
		String name = "";
		for (State s : states)
			name = s.getName() + name;
		
		return name;
	}
	
	/**
	 * 
	 * @param Set<State> states
	 * @return boolean, if the set states contains at least one final state
	 */
	private boolean containsStateFinal(Set<State> states) {
		for (State s : states) {
			for (State stateFinal : finalStates())
				if (s.equals(stateFinal)) return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param Set<State> states
	 * @param boolean isInitial
	 * @return State, it is the new state
	 */
	private State buildState(Set<State> states, boolean isInitial) {
		State newState = new State(generateName(states), isInitial ,containsStateFinal(states));
		return newState;
	}
	
	/**
	 * 
	 * @param Set<State> states
	 * @param Character c
	 * @return Set<State>, the set of states that i move from states to c
	 */
	private Set<State> movements(Set<State> states, Character c) {
		Set<State> statesMovements = new HashSet<State>();
		for (State s : states) {
			if (this.delta.get(s) != null && this.delta.get(s).get(c) != null)
				statesMovements.addAll(this.delta.get(s).get(c));
		}
		
		return statesMovements;
	}

	@Override
	public String toDot() {
		// First part: Write the two initial lines of the .dot file and the
		// States initiales
		String firstPart = "digraph {" + "\n" + "    inic[shape=point];" + "\n" + "    inic->"
				+ initialState().getName() + ";" + "\n";

		// Second part: Write all transitions
		String secondPart = "";
		for (Entry<State, HashMap<Character, Set<State>>> entry : delta.entrySet()) {
			for (Entry<Character, Set<State>> entryAux : entry.getValue().entrySet()) {
				for (State state : entryAux.getValue())
					secondPart = "    " + entry.getKey().getName() + "->" + state.getName() + " [label=\""
							+ entryAux.getKey().toString() + "\"];" + "\n" + secondPart;
			}
		}

		//Third part: Write all the final statements
		String thirdPart = "";
		Set<State> finalStates = finalStates();
		for (State s : finalStates)
			thirdPart = "    " + s.getName() + "[shape=doublecircle];" + "\n" + thirdPart;
		thirdPart = thirdPart + "}";

		return (firstPart + secondPart + thirdPart);

	}

	/*
	 * Automata methods
	 */

	@Override
	public boolean accepts(String string) {
		
		if (string.length() == 0) return false;
		
		if (!verifyString(string)) return false;
		
		int cont = 1;
		LinkedList<Set<State>> conjuntStates = new LinkedList<Set<State>>();
		
		Set<State> statesCurrent = new HashSet<State>();
		statesCurrent.add(initialState());
		conjuntStates.add(movements(statesCurrent, string.charAt(0)));

		while (cont < string.length()) {
			Set<State> setCurrent = conjuntStates.get(0);
			Set<State> movementsCurrent = movements(setCurrent, string.charAt(cont));
			conjuntStates.remove(0);
			conjuntStates.add(movementsCurrent);
			cont++;
		}
		
		if (containsStateFinal(conjuntStates.get(0))) return true;
	
		return false;
	}

	/**
	 * Converts the automaton to a DFA.
	 * 
	 * @return DFA recognizing the same language.
	 */
	public DFA toDFA() {
		Set<State> statesDFA = new HashSet<State>();
		Set<Triple<State, Character, State>> transitions = new HashSet<Triple<State, Character, State>>();
		
		LinkedList<Set<State>> conjuntStates = new LinkedList<Set<State>>();
		
		Set<State> statesCurrent = new HashSet<State>();
		statesCurrent.add(initialState());
		
		conjuntStates.add(statesCurrent);
		
		State initial = buildState(statesCurrent, true);
		statesDFA.add(initial);
		
		while (!conjuntStates.isEmpty()) {
			Set<State> setCurrent = conjuntStates.get(0);
			State stateCurrent = buildState(setCurrent, false);
			
			for (Character c : this.alphabet) {
				Set<State> movementsCurrent = movements(setCurrent, c);
				if (!conjuntStates.contains(movementsCurrent) && !movementsCurrent.isEmpty())
					conjuntStates.add(movementsCurrent);
				State state = buildState(movementsCurrent, false);
				statesDFA.add(state);
				transitions.add(new Triple(stateCurrent, c, state));
			}
			
			conjuntStates.remove(0);
		}
		
		return new DFA(statesDFA, this.alphabet, transitions);
	}

	@Override
	public boolean repOk() {
		int cont = 0;
		for (State s: states) {
			if (s.isInitial()) cont++;
		}
		if (cont == 0 || cont > 1) return false;
		
		//Verify that the transitions are valid (States and valid character)
		for (Entry<State, HashMap<Character, Set<State>>> entry : delta.entrySet()) {
			if (!states.contains(entry.getKey())) return false;
			for (Entry<Character, Set<State>> entryAux : entry.getValue().entrySet()) {
				if (!entryAux.getKey().equals(this.Lambda)) {
					if (!alphabet.contains(entryAux.getKey())) return false;
					for (State state : entryAux.getValue()) {
						if (!states.contains(state)) return false;
					}
				}else {
					// If there is a lambda character
					return false;
				}
			}
		}
		
		return true;
	}
}
