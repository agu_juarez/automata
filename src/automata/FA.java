package automata;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import utils.Triple;

public abstract class FA {

	public static final Character Lambda = '_';

	protected Set<State> states;

	protected Set<Character> alphabet;

	// All states in the map must belong to states set and all symbols must belong
	// to the alphabet
	protected HashMap<State, HashMap<Character, Set<State>>> delta;

	/* Creation */

	/**
	 * Parses and returns a finite automaSet<State> finalStates;ton from the given
	 * file. The type of the automaton returned is the appropriate one for the
	 * automaton represented in the file (i.e. if the file contains the
	 * representation of an automaton that is non-deterministic but has no lambda
	 * transitions, then an instance of NFA must be returned).
	 * 
	 * @param path
	 *            Path to the file containing the specification of an FA.
	 * @return An instance of DFA, NFA or NFALambda, corresponding to the automaton
	 *         represented in the file.
	 * @throws Exception
	 *             Throws an exception if there is an error during the parsing
	 *             process.
	 */
	public static FA parseFromFile(String path) throws Exception {
		Set<State> states = new HashSet<State>();
		Set<Character> alphabet = new HashSet<Character>();
		Set<Triple<State, Character, State>> transitions = new HashSet<Triple<State, Character, State>>();
		State inicial = null;

		path = "/home/agustin/Documents/Automatas/automata/" + path + ".dot";

		File archivo = new File(path);
		FileReader fr = new FileReader(archivo);
		BufferedReader br = new BufferedReader(fr);

		// Reading the file
		String linea;
		while ((linea = br.readLine()) != null) {
			// Search of state initial
			if (linea.contains("inic->")) {
				String nombre = linea.substring(linea.indexOf(">") + 1, linea.indexOf(";"));
				inicial = new State(nombre, true, false);
				states.add(inicial);
			}

			// Search of transitions
			if (linea.contains("label")) {
				// get transition data
				String[] parts = linea.split("->");
				String origen = parts[0].trim();
				String destino = parts[1].substring(0, parts[1].indexOf("[")).trim();
				Character alp = (linea.substring(linea.indexOf("=") + 2, linea.indexOf("=") + 3)).charAt(0);

				State stateOri = new State(origen, false, false);
				State stateDes = new State(destino, false, false);
				Triple<State, Character, State> t = new Triple<State, Character, State>(stateOri, alp, stateDes);

				if (!alp.equals(Lambda))
					alphabet.add(alp);
				if (!states.contains(stateOri))
					states.add(stateOri);
				if (!states.contains(stateDes))
					states.add(stateDes);

				transitions.add(t);
			}

			// Search of states final
			if (linea.contains("doublecircle")) {
				String nombre = linea.substring(linea.indexOf("q"), linea.indexOf("["));
				State fin = new State(nombre, false, true);
				if (states.contains(fin))
					searchAndSetState(states, nombre, true);
				else
					states.add(fin);
			}
			
		}
		
		// Check if the automatas is NFALambda
		if (isNFALambda(transitions))
			return (new NFALambda(states, alphabet, transitions));

		// Check if the automation is AFD or NFA
		else {
			Set<Triple<State, Character, State>> aux = new HashSet<Triple<State, Character, State>>();
			for (Triple<State, Character, State> t : transitions) {
				aux = getTripleEqualsFirstSecond(t, transitions);
				if (aux.size() != 0) 
					return (new NFA(states, alphabet, transitions));
			}

			return new DFA(states, alphabet, transitions);
		}

	}

	/**
	 * Check if there is a Lambda transition between two states
	 * 
	 * @param transitions
	 * @return true if contains transition lambda
	 */
	public static boolean isNFALambda(Set<Triple<State, Character, State>> transitions) {
		for (Triple<State, Character, State> t : transitions)
			if (t.second() == Lambda)
				return true;

		return false;
	}
	
	/**
	 * 
	 * @param triple
	 * @param transitions
	 * @return Set<Triple<State,Character,State>>, que representa todas aquellas
	 *         tripletas que su First y Second son iguales al First y Second de
	 *         triple (no tiene en cuenta triple)
	 */
	private static Set<Triple<State, Character, State>> getTripleEqualsFirstSecond(
			Triple<State, Character, State> triple, Set<Triple<State, Character, State>> transitions) {
		Set<Triple<State, Character, State>> aux = new HashSet<Triple<State, Character, State>>();
		for (Triple<State, Character, State> t : transitions) {
			if (!triple.equals(t))
				if (triple.first().equals(t.first()) && triple.second().equals(t.second()))
					aux.add(t);
		}
		return aux;
	}

	// Search for a given state and modify its isFinal field
	private static void searchAndSetState(Set<State> states, String name, boolean isFinal) {
		for (State s : states) {
			if (s.getName().equals(name)) 
				s.setFinal(isFinal);
		}
	}

	/**
	 * Save given automaton dot description in the specified file
	 * 
	 * @param path
	 *            Path to the file to save a FA.
	 * @throws Exception
	 *             Throws an exception if there is an error during writing file.
	 */
	public static void writeToFile(String path, String dotCode) throws Exception {
		File archivo = new File(path);
		FileWriter fw = new FileWriter(archivo);
		BufferedWriter bw = new BufferedWriter(fw);

		bw.write(dotCode);

		bw.close();
	}

	/**
	 * @return Returns the DOT code representing the automaton.
	 */
	public abstract String toDot();

	/**
	 * @return the atomaton's set of states.
	 */
	public Set<State> getStates() {
		return states;
	}

	/**
	 * @return the atomaton's alphabet.
	 */
	public Set<Character> getAlphabet() {
		return alphabet;
	}

	/**
	 * @return the atomaton's initial state.
	 */
	public State initialState() {
		for (State s : this.states) {
			if (s.isInitial())
				return s;
		}
		return null;
	}

	/**
	 * @return the atomaton's final states.
	 */
	public Set<State> finalStates() {
		Set<State> finalStates = new HashSet<State>();
		for (State s : this.states) {
			if (s.isFinal())
				finalStates.add(s);
		}
		return finalStates;
	}

	/**
	 * Query for the automaton's transition function.
	 * 
	 * @return A set of states (when FA is a DFA this method return a singleton set)
	 *         corresponding to the successors of the given state via the given
	 *         character according to the transition function.
	 */
	public Set<State> delta(State from, Character c) {
		assert states.contains(from);
		assert alphabet.contains(c);

		Set<State> s = new HashSet<State>();
		if (delta.containsKey(from)) {
			HashMap<Character, Set<State>> value = delta.get(from);
			if (value.containsKey(c))
				s = value.get(c);
		}

		return s;
	}

	/*
	 * Automata Methods
	 */

	/**
	 * Tests whether a string belongs to the language of the current finite
	 * automaton.
	 * 
	 * @param string
	 *            String to be tested for acceptance.
	 * @return Returns true iff the current automaton accepts the given string.
	 */
	public abstract boolean accepts(String string);

	/**
	 * Verifies whether the string is composed of characters in the alphabet of the
	 * automaton.
	 * 
	 * @return True iff the string consists only of characters in the alphabet.
	 */
	public boolean verifyString(String s) {
		int length = s.length();
		for (int i = 0; i < length; i++) {
			if (!alphabet.contains(s.charAt(i)))
				return false;
		}

		return true;
	}

	/**
	 * @return True iff the automaton is in a consistent state.
	 */
	public abstract boolean repOk();

}
